# JOD PC Windows - 1.0

Documentation for JOD PC Windows. This JOD Distribution allow to startup and manage a JOD Agent that represent a Windows computer.


## JOD Distribution Specs

This JOD Distribution was created and maintained as part of the John O.S. Project.

|||
|---|---|
| **Current version**    | 1.0
| **References**        | [JOD PC Windows @ JOSP Docs](https://www.johnosproject.org/docs/references/jod_dists/jod_pc_win/)
| **Repository**        | [com.robypomper.josp.jod.pc.windows @ Bitbucket](https://bitbucket.org/johnosproject_shared/com.robypomper.josp.jod.pc.windows/)
| **Downloads**            | [com.robypomper.josp.jod.pc.windows > Downloads @ Bitbucket](https://bitbucket.org/johnosproject_shared/com.robypomper.josp.jod.pc.windows/downloads/)

This distribution can be installed on a Windows computer, and then it starts sharing computer's features to the JOSP EcoSystem like te volume control, resources (cpu, memory, disks...) states, etc...

This distribution can be configured to use listeners or pullers as states executors. That means if the distribution is configured to use ```pullers``` as executors, then it executes a powershell script each time it poll a state's value (it use [ShellPuller](https://www.johnosproject.org/docs/references/josp/jod/specs/workers/puller_shell). Otherwise, if you set ```listeners``` executors, before starting the JOD instance, the pre-startup script launch a powershell script for each state configured in the ```struct.jod```. Those scripts run in background for all time the JOD instance is running. They are delegated to read states values and write them on corresponding file, so the JOD instance can read them via [Listener File](https://www.johnosproject.org/docs/references/josp/jod/specs/workers/listener_file).

**NB:** Powershell execution consume many resources, please use the ```listeners``` states.

In the ```configs/configs.ps1``` file you can find and configure following properties:

| Property | Example | Description |
|----------|---------|-------------|
| **PC_WINDOWS_STATES_TYPE** | listeners or pullers  | Select the state's executors type |

## JOD Distribution Usage

### Locally JOD Instance

Each JOD Distribution comes with a set of script for local JOD Instance management.

| Command | Description |
|---------|-------------|
| Start    <br/>```$ bash start.sh```     | Start local JOD instance in background mode, logs can be retrieved via ```tail -f logs/console.log``` command |
| Stop     <br/>```$ bash stop.sh```      | Stop local JOD instance, if it's running |
| State    <br/>```$ bash state.sh```     | Print the local JOD instance state (obj's id and name, isRunning, PID...) |
| Install  <br/>```$ bash install.sh```   | Install local JOD instance as system daemon/service |
| Uninstall<br/>```$ bash uninstall.sh``` | Uninstall local JOD instance as system daemon/service |

### Remote JOD Instance

To deploy and manage a JOD instance on remote device (local computer, cloud server,
object device...) please use the [John Object Remote](https://www.johnosproject.org/docs/references/tools/john_object_remote/)
tools.