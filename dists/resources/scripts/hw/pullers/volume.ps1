Param(
    [parameter(Mandatory = $false)][String]$Volume
)

.$PSScriptRoot\..\audio_apis.ps1

if ($Volume -eq "true")
{
    [audio]::Mute = $true
    exit
}
elseif ($Volume -eq "false")
{
    [audio]::Mute = $false
    exit
}
elseif ($Volume -ne "")
{
    [audio]::Volume = [int]$Volume/100
    exit
}

[int]($( [audio]::Volume ) * 100)
