# Script params customize first param to use this script with different values
# #############
Param(
    [parameter(Mandatory = $false)][String]$Volume = "",
    [parameter(Mandatory = $false)][String]$File = "$PSScriptRoot/tmp/volume_mute.txt"
)
$ParamValue = $Volume


# Override following methods to customize this script to different values
# ##########################

# Init Audio APIs
.$PSScriptRoot\..\audio_apis.ps1

function getValue()
{
    [audio]::Mute
}

function setValue($val)
{
    if ($val -eq "true")
    {
        [audio]::Mute = $true
    }
    elseif ($val -eq "false")
    {
        [audio]::Mute = $false
    }
}

function getSleepTime()
{
    1
}


# Start script execution
# ######################

# Set value and exit
if ($ParamValue -ne "")
{
    write-host "Value $ParamValue"
    setValue($ParamValue)
    exit
}

# Loop for get value and write on file
while ($true)
{
    $val = getValue
    Set-Content -Path $File $val
    Start-Sleep $( getSleepTime )
}

[int]($( [audio]::Volume ) * 100)