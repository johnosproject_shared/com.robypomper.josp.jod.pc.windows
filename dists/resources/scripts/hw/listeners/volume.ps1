# Script params customize first param to use this script with different values
# #############
Param(
    [parameter(Mandatory = $false)][String]$Volume = "",
    [parameter(Mandatory = $false)][String]$File = "$PSScriptRoot/tmp/volume.txt"
)
$ParamValue = $Volume


# Override following methods to customize this script to different values
# ##########################

# Init Audio APIs
.$PSScriptRoot\..\audio_apis.ps1

function getValue()
{
    [int]($( [audio]::Volume ) * 100)
}

function setValue($val)
{
    [audio]::Volume = [int]$val/100
}

function getSleepTime()
{
    1
}


# Start script execution
# ######################

# Set value and exit
if ($ParamValue -ne "")
{
    setValue($ParamValue)
    exit
}

# Loop for get value and write on file
while ($true)
{
    $val = getValue
    Set-Content -Path $File $val
    Start-Sleep $( getSleepTime )
}

[int]($( [audio]::Volume ) * 100)