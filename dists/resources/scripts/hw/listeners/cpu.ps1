# Script params customize first param to use this script with different values
# #############
Param(
    [parameter(Mandatory = $false)][String]$File = "$PSScriptRoot/tmp/mem_used.txt"
)


# Override following methods to customize this script to different values
# ##########################

# Init Audio APIs
.$PSScriptRoot\audio_apis.ps1

function getValue()
{
    (Get-Counter '\Processor(_Total)\% Processor Time').CounterSamples.CookedValue
}

function getSleepTime()
{
    30
}


# Start script execution
# ######################

# Loop for get value and write on file
while ($true)
{
    $val = getValue
    Set-Content -Path $File $val
    Start-Sleep $( getSleepTime )
}
