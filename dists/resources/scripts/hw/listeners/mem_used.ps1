# Script params customize first param to use this script with different values
# #############
Param(
    [parameter(Mandatory = $false)][String]$File = "$PSScriptRoot/tmp/mem_used.txt"
)


# Override following methods to customize this script to different values
# ##########################

# Init Audio APIs
.$PSScriptRoot\audio_apis.ps1

function getValue()
{
    $usedMem = ((Get-Counter '\Memory\Committed Bytes').CounterSamples.CookedValue)/1024/1024
    $usedMem.ToString("N0")
}

function getSleepTime()
{
    30
}


# Start script execution
# ######################

# Loop for get value and write on file
while ($true)
{
    $val = getValue
    Set-Content -Path $File $val
    Start-Sleep $( getSleepTime )
}
