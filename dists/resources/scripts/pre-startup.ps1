#!/usr/bin/env powershell

################################################################################
# The John Operating System Project is the collection of software and configurations
# to generate IoT EcoSystem, like the John Operating System Platform one.
# Copyright (C) 2021 Roberto Pompermaier
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
################################################################################

###############################################################################
# Customization:
# $JOD_DIR/scripts/pre-startup.ps1
#
# This script is executed before JOD instance startup (via start.sh script).
#
# It can be customized adding more checks or operations depending on
# JOD Distribution needs.
#
#
# Artifact: JOD Dist Template
# Version:  1.0
###############################################################################

## Default init - START
$JOD_DIR = (get-item $PSScriptRoot).Parent.FullName
.$JOD_DIR/scripts/libs/include.ps1 "$JOD_DIR"

#$DEBUG=$true
if (($null -ne $DEBUG) -and ($DEBUG))
{
    INSTALL-LogsDebug
}
else
{
    INSTALL-Logs
}
setupCallerAndScript $PSCommandPath $MyInvocation.PSCommandPath

."$JOD_DIR/scripts/jod/jod-script-configs.ps1"
execScriptConfigs "$JOD_DIR/scripts/jod/errors.ps1"

# Load jod_configs.sh, exit if fails
setupJODScriptConfigs "$JOD_DIR/configs/configs.ps1"
## Default init - END

logInf "PRE Startup script"

# Check supported OS
$supportedOS = "Win32"
failOnUnsupportedOS $supportedOS

Copy-Item -Path "$JOD_DIR/configs/struct_$PC_WINDOWS_STATES_TYPE.jod" -Destination "$JOD_DIR/configs/struct.jod"

function startStatusBackgroundProcess($scriptFilePath, $scriptOutputFilePath)
{
    Start-Process "powershell.exe" -WindowStyle Hidden -ArgumentList "$scriptFilePath", "-File", "$scriptOutputFilePath"
}

if ($PC_WINDOWS_STATES_TYPE -eq "listeners")
{
    New-Item -Path "$JOD_DIR" -Name "status" -ItemType "directory" -ea 0
    startStatusBackgroundProcess "$JOD_DIR/scripts/hw/listeners/volume.ps1" "$JOD_DIR/status/volume.txt"
    startStatusBackgroundProcess "$JOD_DIR/scripts/hw/listeners/volume_mute.ps1" "$JOD_DIR/status/volume_mute.txt"
    startStatusBackgroundProcess "$JOD_DIR/scripts/hw/listeners/mem_free.ps1" "$JOD_DIR/status/mem_free.txt"
    startStatusBackgroundProcess "$JOD_DIR/scripts/hw/listeners/mem_used.ps1" "$JOD_DIR/status/mem_used.txt"
    startStatusBackgroundProcess "$JOD_DIR/scripts/hw/listeners/cpu.ps1" "$JOD_DIR/status/cpu.txt"
}
